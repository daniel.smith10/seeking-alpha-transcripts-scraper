import torch
from transformers import AutoTokenizer, AutoModel


class CosineSimilarity:
    def __init__(self, model_name):
        self.tokenizer = AutoTokenizer.from_pretrained(model_name, model_max_length=512)
        self.model = AutoModel.from_pretrained(model_name)

    def get_embeddings(self, text):
        inputs = self.tokenizer(text, return_tensors='pt', padding=True, truncation=True)
        with torch.no_grad():
            outputs = self.model.encoder(**inputs, return_dict=True)

        embeddings = torch.mean(outputs.last_hidden_state, dim=1)  # Average pooling
        return torch.nn.functional.normalize(embeddings)

    def get_cosine_similarity(self, text1, text2):
        embeddings1 = self.get_embeddings(text1)
        embeddings2 = self.get_embeddings(text2)
        similarity = torch.nn.functional.cosine_similarity(embeddings1, embeddings2)
        return similarity.item()
    