import re
import time

import aiohttp
import asyncio

import json

from bs4 import BeautifulSoup
from loguru import logger
from pb_ml.common.paths import ensure_dir_exists


class SeekingAlphaTranscriptScraper:
    base_url = """https://seekingalpha.com"""

    def __init__(self) -> None:
        """
        Initialize the scraper
        """
        self.data_dir = ensure_dir_exists("transcripts")

    async def process_list_page(self, page_index: int) -> None:
        origin_page = f"https://seekingalpha.com/earnings/earnings-call-transcripts?page={page_index}"
        logger.info(f"getting page {origin_page}")
        soup = await self.get_website_soup_object(origin_page)
        transcript_links = self.get_transcript_list(soup)
        tasks = []
        for link in transcript_links:
            task = asyncio.create_task(self.get_transcript_title_and_content(link))
            tasks.append(task)

        await asyncio.gather(*tasks)

    @staticmethod
    async def get_website_soup_object(target_url: str):
        """
        Get the soup object for a given url
        :param target_url:
        :return:
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(target_url) as response:
                content = await response.read()

        return BeautifulSoup(content, 'html.parser')

    def get_transcript_list(self, origin_page_soup: BeautifulSoup) -> list:
        json_obj = self.get_article_json(origin_page_soup)
        try:
            transcript_list = [f"{self.base_url}{article_json['links']['self']}" for article_json in
                               json_obj['articles']['response']['data']]
        except KeyError:
            raise(KeyError("Could not find the article list"))

        return transcript_list

    @staticmethod
    def get_article_json(article_soup) -> dict:
        scripts = article_soup.find_all("script")
        raw_text = None
        for script in scripts:
            raw_text = re.findall(r'''\{"article.*$''', script.text, re.MULTILINE)
            if raw_text:
                raw_text = raw_text[0].rstrip(";")
                break

        if not raw_text:
            return {}

        # noinspection PyTypeChecker
        json_obj = json.loads(raw_text)

        return json_obj

    async def get_transcript_title_and_content(self, url: str) -> None:
        logger.info(f"Processing page: {url}")
        soup = await self.get_website_soup_object(url)
        json_obj = self.get_article_json(soup)
        try:
            content = json_obj['article']['response']['data']['attributes'].get('content')
            title = json_obj['article']['response']['data']['attributes'].get('title')
        except KeyError:
            logger.debug(f"Could not find the article content: {url}")

            return

        if "Earnings Call Transcript" in title:
            ticker = self.get_ticker(title)
            quarter = self.get_quarter(title)
            year = self.get_year(title)
            company_name = self.get_company_name(title)
            exchange = self.get_exchange(content)
            if exchange in ("NASDAQ", "NYSE"):
                file_name = f"{ticker}_{exchange}_{quarter}_{year}_{company_name}"
                self.write_transcript_content_to_file(content, file_name)

    @staticmethod
    def get_company_name(text):
        regex = r"(.*?)\("
        match = re.search(regex, text)

        company_name = "noCompany"
        if match:
            company_name = match.group(1)

        company_name = company_name.rstrip().replace(" ", "_").lower()
        company_name = re.sub(r"[^a-zA-Z0-9_]+", '', company_name)

        return company_name

    @staticmethod
    def get_ticker(text):
        regex = r"\b([A-Z]+)\b"
        match = re.search(regex, text)

        ticker = "noTicker"
        if match:
            ticker = match.group()

        return ticker # .lower()

    @staticmethod
    def get_quarter(text):
        regex = r"Q[1-4]"
        match = re.search(regex, text)
        quarter = "noQuarter"
        if match:
            quarter = match.group()

        return quarter # .lower()

    @staticmethod
    def get_year(text):
        regex = r"20\d{2}"
        match = re.search(regex, text)
        year = "noYear"
        if match:
            year = match.group()

        return year

    @staticmethod
    def get_exchange(text):
        regex = r">([^<>]+?):[A-Z]*<"
        match = re.search(regex, text)
        exchange = "no_exchange"
        if match:
            exchange = match.group(1)

        return exchange # .lower()

    @staticmethod
    def get_call_start_paragraph_index(paragraphs):
        index = 0
        for index, paragraph in enumerate(paragraphs):
            if paragraph.text == "Operator":
                break
        return index + 1

    @staticmethod
    def make_clean_paragraphs(call_paragraphs):
        texts = []
        for para in call_paragraphs:
            strong_tags = para.find_all('strong')
            if not strong_tags:
                text = para.text.strip()
                text = text.replace('\n', '')
                text = re.sub(r"\\'", "'", text)
                text = re.sub(r'\s+', ' ', text)
                texts.append(text.lower() + '\n\n')

        return "".join(texts)

    def write_transcript_content_to_file(self, json_data, file_name):
        soup = BeautifulSoup(json_data, 'html.parser')
        paragraphs = soup.find_all("p")
        start_index = self.get_call_start_paragraph_index(paragraphs)
        call_paragraphs = paragraphs[start_index:]
        text = self.make_clean_paragraphs(call_paragraphs)

        with open(f"{self.data_dir}/{file_name}.txt", "w") as fstream:
            fstream.write(text)


if __name__ == "__main__":
    start = time.perf_counter()
    scraper = SeekingAlphaTranscriptScraper()
    for page_index in range(1, 50):
        asyncio.run(scraper.process_list_page(page_index))
        time.sleep(30)
    finish = time.perf_counter() - start
    print(f"Finished in {finish} seconds")
